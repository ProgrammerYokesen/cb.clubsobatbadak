<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use cookie;

class EmailController extends Controller
{
  public function validateemail(){
    $pull = DB::table('users')->where('emailValidation','new')->where('spam','<',2)->limit(5)->get();
    $client = new Client();
    foreach($pull as $n => $isi){

      $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$isi->email.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
      $res = json_decode($result->getBody());
      $check = $res->mailbox_verification;
      //dd($check);
      if($check == "true"){
        $update = DB::table('users')->where('id',$isi->id)->update([
          'emailValidation' => 'true'
        ]);
        echo $isi->email." --- true <br>";
      }else{
        $update = DB::table('users')->where('id',$isi->id)->update([
          'emailValidation' => 'invalid'
        ]);
        echo $isi->email." --- invalid <br>";
      }
      sleep(5);
    }
  }

  public function export_true_email(){
      $date = date('d-m-Y');
      $file = 'laporan Data Email True - '.$date;

      Excel::create($file, function($excel) {

        $excel->sheet('True', function($sheet) {
          $users = DB::table('user')->where('emailValidation','true')->get();
          $sheet->loadView('campaign.excell.email-true',compact('users'));
        });
      })->store('xls', storage_path('excel/exports'))->export('xls');

      return redirect()->back();
    }

}
