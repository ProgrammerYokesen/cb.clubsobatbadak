<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Jenssegers\Agent\Agent;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $agent = new Agent();
        $device = $agent->device();
        $ipaddress = \Request::ip();

        $kota = $data['kota'];

        $pullKota = DB::table('regencies')->where('id',$kota)->first();
        $province = $pullKota->province_id;
        $area = $pullKota->area_id;

        $now = date('Y-m-d H:i:s',strtotime('+ 7 hours'));

        if($now < date('2021-05-06 00:00:00')){
          if($data['ref'] != ''){
            $ref = DB::table('users')->where('id',$data['ref'])->first();
            $update = DB::table('users')->where('id',$data['ref'])->update([
              'countRef' => $ref->countRef + 1
            ]);
          }
        }

        $requ = DB::table('users')->where('id',$data['ref'])->first();
        if($requ->spam <1 ){
          return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'whatsapp' => $data['whatsapp'],
            'kota' => $data['kota'],
            'provinsi' => $province,
            'area' => $area,
            'ref_id' => $data['ref'],
            'ipaddress' => $ipaddress,
            'device' => $device,
            'subscribeEmail' => $data['subscribeEmail'],
            'subscribeWhatsapp' => $data['subscribeWhatsapp'],
            'utm_campaign' => $data['utm_campaign'],
            'utm_source' => $data['utm_source'],
            'utm_medium' => $data['utm_medium'],
            'utm_content' => $data['utm_content']
          ]);
        }else{
          return redirect()->route('masuk');
        }



    }
}
