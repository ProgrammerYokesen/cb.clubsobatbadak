<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use Input;
use Auth;
use Cookie;
use CRUDBooster;

class PagesController extends Controller
{

    public function leadform(){
      if(Input::get('ref')){
        Cookie::queue('ref', Input::get('ref'), 3243200);
        $ref_id = Input::get('ref');

      }else{
        $ref_id = Cookie::get('ref');
      }

      if(Input::get('utm_campaign')){
        Cookie::queue('utm_campaign', Input::get('utm_campaign'), 3243200);
        $utm_campaign = Input::get('utm_campaign');

      }else{
        $utm_campaign = Cookie::get('utm_campaign');
      }

      if(Input::get('utm_medium')){
        Cookie::queue('utm_medium', Input::get('utm_medium'), 3243200);
        $utm_medium = Input::get('utm_medium');

      }else{
        $utm_medium = Cookie::get('utm_medium');
      }

      if(Input::get('utm_source')){
        Cookie::queue('utm_source', Input::get('utm_source'), 3243200);
        $utm_source = Input::get('utm_source');

      }else{
        $utm_source = Cookie::get('utm_source');
      }

      if(Input::get('utm_content')){
        Cookie::queue('utm_content', Input::get('utm_content'), 3243200);
        $utm_content = Input::get('utm_content');

      }else{
        $utm_content = Cookie::get('utm_content');
      }


        $currentDate = Carbon::now()->format('Y-m-d');

        $getTanggal = DB::table('studio_tausiah')->where('tanggal', '>' ,date('Y-m-d',strtotime('- 1 day')))->first();

        //$studioTausiah = DB::table('studio_tausiah')->get();
        $studioTausiah = DB::table('studio_tausiah')->where('tanggal', $getTanggal->tanggal)->get();

        // studio hiburan
      //  $studioHiburan = DB::table('studio_hiburan')->get();
       $studioHiburan = DB::table('studio_hiburan')->where('tanggal', $getTanggal->tanggal)->get();

        // studio lounge
        //$studioLounge = DB::table('studio_lounge')->get();
         $studioLounge = DB::table('studio_lounge')->where('tanggal', $getTanggal->tanggal)->get();

        // Link
        $link = DB::table('link')->first();

        // pembicara
        $pembicara = DB::table('pembicara')->get();
        // dd($pembicara);

        // webinar
         $webinar = DB::table('webinar')->where('tanggal', $getTanggal->tanggal)->get();
        //$webinar = DB::table('webinar')->get();

        // faq
        $faq = DB::table('faq')->get();

        $questions = DB::table('pertanyaan_rejeki_nomplok')->orderby('id','desc')->paginate(5);

        return view('pages.lead', compact('studioTausiah', 'studioHiburan', 'studioLounge', 'link', 'pembicara', 'webinar', 'faq','ref_id','utm_medium','utm_source','utm_campaign','utm_content', 'getTanggal','questions'));

    }

    public function homePage() {
      return route(CRUDBooster::mainpath($slug=NULL));
    }

  
}
