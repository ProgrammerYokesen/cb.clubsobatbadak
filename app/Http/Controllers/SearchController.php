<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SearchController extends Controller
{
  public function kota(Request $request){
    if ($request->has('q')) {
          $cari = $request->q;
          $results = DB::table('regencies')->where('name','LIKE','%'.$cari.'%')->get();
          return response()->json($results);
      }
  }
}
