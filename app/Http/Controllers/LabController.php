<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

class LabController extends Controller
{
    public function deteksi_spam(){
      $satujamlalu = date('Y-m-d H:i:s',strtotime('-7 minutes'));
      //$satujamlalu = date('2021-04-10 00:00:00');
      $ref = DB::table('users')->where('ref_id','>',9)->where('created_at','>',$satujamlalu)->distinct()->select('ref_id')->get();
      //dd($satujamlalu,$ref);
      foreach ($ref as $value) {
          $getref = DB::table('users')->where('ref_id',$value->ref_id)->count();
          //$check_spam = DB::table('users')->where('id',$value->ref_id)->first();
          if($getref > 24){
              $ipdistinct = DB::table('users')->where('ref_id',$value->ref_id)->distinct()->select('ipaddress')->get();
              $ipcount = count($ipdistinct);
              $selisih = $getref - $ipcount;
              $ratio = $selisih/$getref *100;
              if($ratio>50){
                $total += $getref;
                echo $value->ref_id." --> ".$getref." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";
                $update = DB::table('users')->where('id',$value->ref_id)->update([
                  'spam' => 1
                ]);
                $update = DB::table('users')->where('ref_id',$value->ref_id)->update([
                  'spam' => 2
                ]);
              }
          }
      }
      $totaluser = DB::table('users')->count();
      $percentuser = $total/$totaluser*100;
      echo "<br>-------------------------------------<br>Count Spam data : ".$total."<br>";
      echo "Total Data Register : ".$totaluser."<br>";
      echo "Percent spam from total data : ".number_format($percentuser,2,'.','.')." %";
    }

    public function deteksi_ratio(){
      $leaderboards = DB::table('users')->where('spam','<',1)->where('id','>',9)->where('id','!=',395)->where('id','!=',556)->where('id','!=',636)->where('id','!=',633)->orderby('countRef','desc')->limit(25)->get();
      foreach($leaderboards as $n => $leaderboard){
        $refcount = DB::table('users')->where('ref_id',$leaderboard->id)->count();
        $ipget = DB::table('users')->where('ref_id',$leaderboard->id)->distinct()->select('ipaddress')->get();
        $ipcount = count($ipget);
        $selisih = $refcount - $ipcount;
        $ratio = $selisih/$refcount *100;

        $emailtrue = DB::table('users')->where('ref_id',$leaderboard->id)->where('emailValidation','true')->count();
        $ratioEmail = $emailtrue/$refcount*100;

        $emailfalse = DB::table('users')->where('ref_id',$leaderboard->id)->where('emailValidation','invalid')->count();
        $ratioEmailfalse = $emailfalse/$refcount*100;

        $update = DB::table('users')-where('id',$leaderboard->id)->update([
          'spamRatio' => $ratio,
          'emailTrue' => $ratioEmail,
          'emailFalse' => $ratioEmailfalse
        ]);
      }
    }

    public function perhour(){
      $start = date('2021-04-10 00:00:00');
      $end = date('Y-m-d H:i:s',strtotime('+1day'));
      $hourdiff = round((strtotime($end) - strtotime($start))/(3600*24), 0);
      for ($i=0; $i < $hourdiff ; $i++) {
        $open = date('Y-m-d H:i:s',strtotime($start.'+ '.($i*24).' hour'));
        $end = date('Y-m-d H:i:s',strtotime($start.'+ '.($i*24).' hour + 24 hour'));
        $count = DB::table('users')->where('created_at','>',$open)->where('created_at','<',$end)->count();
        $clean = DB::table('users')->where('created_at','>',$open)->where('created_at','<',$end)->where('spam',0)->count();
        $data[] = [
          'label' => date('d-m',strtotime($open.'+7 hours')),
          'count' => $count,
          'clean' => $clean
        ];
      }

/*
      $end2 = date('Y-m-d H:i:s');
      $start2 = date('2021-04-10 00:00:00');

      $hourdiff2 = round((strtotime($end2) - strtotime($start2))/(3600*4), 0);
      for ($i=0; $i < $hourdiff2 ; $i++) {
        $open2 = date('Y-m-d H:i:s',strtotime($start2.'+ '.($i*4).' hours'));
        $end2 = date('Y-m-d H:i:s',strtotime($start2.'+ '.($i*4).' hours + 4 hours'));
        $count2 = DB::table('users')->where('created_at','>',$open2)->where('created_at','<',$end2)->count();
        $clean2 = DB::table('users')->where('created_at','>',$open2)->where('created_at','<',$end2)->where('spam',0)->count();

        $hourdata[] = [
          'label' => date('d-m [H]',strtotime($open2.'+7 hours')),
          'count' => $count2,
          'clean' => $clean2
        ];
      }
*/
$hourdata = [];
      $rightNow = date('Y-m-d H:i:s');
      $startDaily = date('Y-m-d H:i:s',strtotime($rightNow.'-1 days'));
      $dayDiff = round((strtotime($rightNow) - strtotime($startDaily))/(60 * 60 * 24), 0);

      for ($x=0; $x < $dayDiff; $x++) {
        $openDaily = date('Y-m-d H:i:s',strtotime($startDaily.'+ '.($x*1).' day'));
        $endDaily = date('Y-m-d H:i:s',strtotime($startDaily.'+ '.($x*1).' day + 1 day'));

        $hourdiff2 = round((strtotime($endDaily) - strtotime($openDaily))/(3600), 0);
        $hourdatad = [];
        for ($i=0; $i < $hourdiff2 ; $i++) {
          $open2 = date('Y-m-d H:i:s',strtotime($openDaily.'+ '.($i*1).' hour'));
          $end2 = date('Y-m-d H:i:s',strtotime($openDaily.'+ '.($i*1).' hour + 1 hour'));
          $count2 = DB::table('users')->where('created_at','>',$open2)->where('created_at','<',$end2)->count();
          $clean2 = DB::table('users')->where('created_at','>',$open2)->where('created_at','<',$end2)->where('spam',0)->count();

          $hourdatad[] = [
            'label' => date('[H] ',strtotime($open2.'+7 hours')),
            'count' => $count2,
            'clean' => $clean2
          ];
        }

        $daydata[] = [
          'day' => date('d-m-Y',strtotime($openDaily.'+7 hours')),
          'data' => $hourdatad,
        ];

      }

      return view('pages.velocity-data',compact('data','hourdata','daydata'));
    }

    public function total(){
      $total = DB::table('users')->count();
      $spam = DB::table('users')->where('spam','2')->count();
      $real = DB::table('users')->where('spam','<','2')->count();

      $thisHour = DB::table('users')->where('created_at','>',date('Y-m-d H'))->count();
      $minutes = date('i');
      session::put('real2',Session::get('real1'));
      session::put('real1',$real);

      $totalAvg = $thisHour;

      $avg = number_format($totalAvg/$minutes,0,'.','.');
      $forecast = $avg * 60;

      $percent_spam = $spam/$total*100;
      $iklan = DB::table('users')->where('id','<',10)->get();
/*
      $getcampaign = DB::table('users')->distinct()->select('utm_campaign')->get();

      foreach ($getcampaign as $gcamp) {
        $countCampaign = DB::table('users')->where('utm_campaign',$gcamp->utm_campaign)->count();
        $campaigns[] = [
          'count' => $countCampaign,
          'name' => $gcamp->utm_campaign
        ];
      }

      $getcontent = DB::table('users')->distinct()->select('utm_content')->get();

      foreach ($getcontent as $gcont) {
        $countContent = DB::table('users')->where('utm_content',$gcont->utm_content)->count();
        $contents[] = [
          'count' => $countContent,
          'name' => $gcont->utm_content
        ];
      }

      $getmedium = DB::table('users')->distinct()->select('utm_medium')->get();

      foreach ($getmedium as $gmedium) {
        $countmedium = DB::table('users')->where('utm_medium',$gmedium->utm_medium)->count();
        $mediums[] = [
          'count' => $countmedium,
          'name' => $gmedium->utm_medium
        ];
      }

      $getsource = DB::table('users')->distinct()->select('utm_source')->get();

      foreach ($getsource as $gsource) {
        $countsource = DB::table('users')->where('utm_source',$gsource->utm_source)->count();
        $sources[] = [
          'count' => $countsource,
          'name' => $gsource->utm_source
        ];
      }

      rsort($campaigns);
      rsort($contents);
      rsort($sources);
      rsort($mediums);
*/
      $campaigns = [];
      $mediums = [];
      $sources = [];
      $contents = [];

      return view('pages.total',compact('total','iklan','percent_spam','spam','real','campaigns','contents','sources','mediums','avg','totalAvg','forecast'));
    }

    public function spam_meter(){
      $leaderboards = DB::table('users')->where('spam','<',1)->where('id','>',9)->where('id','!=',395)->where('id','!=',556)->where('id','!=',636)->where('id','!=',633)->orderby('countRef','desc')->limit(10)->get();

      foreach ($leaderboards as $value) {
        $refcount = DB::table('users')->where('ref_id',$value->id)->count();
        $ipget = DB::table('users')->where('ref_id',$value->id)->distinct()->select('ipaddress')->get();
        $ipcount = count($ipget);
        $selisih = $refcount - $ipcount;
        $ratio = $selisih/$refcount *100;

          echo $value->id." --> ".$refcount." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";

      }
    }

}
