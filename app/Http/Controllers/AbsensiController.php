<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
use Auth;
use Carbon\Carbon;


class AbsensiController extends Controller
{
    public function lounge(){
      $today = date('Y-m-d H:i:s');
      $update = DB::table('bingkisan_lebaran')->where('user_id',Auth::user()->id)->update([
        'last_lounge' => $today
      ]);
    }
}
