<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use DB;

class GameController extends Controller
{
  public function game_hangman($id){
      // $kata = ['T','E','R','B','A','I','K'];


      $gameId = Session::get('gameId');
      if($gameId == NULL){
          // dd('masuk if atas');
          // dd($gameId, $id);
          session(['gameId' => $id]);
      }

      elseif($gameId != $id){
          // dd($gameId, $id);
          // dd('masuk else if');
          // dd($gameId, $id);
          Session::flush(); // removes all session data
          session(['gameId' => $id]);
          return redirect()->route('minigame', [$id]);
      }
      // dd('gak masuk manaapun');
      // $gameId = Session::get('gameId');
      // dd($gameId);
      $word = DB::table('game')->where('id', $id)->where('status', 'Active')->first();
      $deskripsi = DB::table('game')->where('id', $id)->pluck('deskripsi')->first();
      $kata = str_split($word->kata);
      $background = DB::table('game')->where('id', $id)->pluck('background')->first();
      $logo = DB::table('game')->where('id', $id)->pluck('logo')->first();

      // dd($logo);
      $tebakan = Session::get('tebak');
      return view('campaign.minigame.app',compact('kata','tebakan', 'id', 'word', 'deskripsi', 'background', 'logo'));
  }

  public function satu_submit(Request $request, $id){
        // $kata = ['T','E','R','B','A','I','K'];
        $word = DB::table('game')->where('id', $id)->where('status', 'Active')->first();
        $kata_split = str_split($word->kata);
        $kata = array_unique($kata_split);


        $huruf = strtoupper($request->huruf);
        // dd($huruf);

        if(empty(Session::get('tebak'))){
            $tebak[0] = $huruf;
            if(in_array($huruf, $kata)){
                $last = "betul";
            }else{
                $last = "salah";
            }
        }else{
            $tebak = Session::get('tebak');
            if (in_array($huruf, $tebak)) {
                $last = Session::get('last');
            }else{
                array_push($tebak,$huruf);
                if(in_array($huruf, $kata)){
                $last = "betul";
            }else{
                $last = "salah";
            }

            }
        }

        $nilai = 0;
        $salah = 0;
        foreach ($tebak as $value) {
            if(in_array($value, $kata)){
                $nilai += 1;
            }else{
                $salah += 1;
            }
        }

        $percent_nilai = $nilai / count($kata);
        $percent_salah = ($nilai - $salah) / count($kata);
        // dd($percent_nilai);


        $request->session()->put('last', $last);
        $request->session()->put('salah', $salah);
        $request->session()->put('nilai', $percent_nilai);
        $request->session()->put('nilai_salah', $percent_salah);
        $request->session()->put('tebak', $tebak);

        return redirect()->route('minigame', [$id]);
    }

    public function satu_reset(){
        Session::flush();
        return redirect()->back();
        // return redirect()->route('minigame', [$id]);
    }
}
