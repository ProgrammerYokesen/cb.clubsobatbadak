<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'whatsapp',
        'ipaddress',
        'kota',
        'provinsi',
        'area',
        'ref_id',
        'ipaddress',
        'device',
        'utm_campaign',
        'utm_source',
        'utm_medium',
        'utm_content',
        'alamat_rumah',
        'subscribeEmail',
        'subscribeWhatsapp'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
