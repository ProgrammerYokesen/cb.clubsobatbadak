'use strict';
var csrfToken = $('[name="csrf_token"]').attr('content');

setInterval(refreshToken, 600000); // 1 hour

function refreshToken(){
  $.get('refresh-csrf').done(function(data){
    csrfToken = data; // the new token
  });
}

setInterval(refreshToken, 600000); // 1 hour
