@extends('template.master')

@section('banner')
  <section class="page-section no-padding background-img-slider">
    <div class="container">

      <div id="main-slider" class="owl-carousel owl-theme">

        <!-- Slide -->
        <div class="item page text-center slide0">
          <div class="caption">
            <div class="container">
              <div class="div-table">
                <div class="div-cell" >
                  <img src="{{ asset('assets/img/logo-acara.png') }}" alt="logo_acara"
                  class="logo_acara">
                  <h3 class="caption-subtitle" data-animation="fadeInDown"
                  data-animation-delay="300">UUPPSS INTERNET KAMU PADAM, COBA DI-REFRESH YA!
                </h3>


                <p class="caption-text">
                  <a class="btn btn-theme btn-theme-xl scroll-to" href="{{route('homePage')}}"
                  data-animation="flipInY" data-animation-delay="600"> KLIK DISINI UNTUK KE HALAMAN DEPAN </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection

@section('content')

@endsection
