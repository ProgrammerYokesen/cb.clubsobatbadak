<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwordnya minimal 6 karakter huruf atau angka. Oh iya, konfirmasi passwordnya harus sama dengan password ya.',
    'reset' => 'Yeay! Password kamu udah direset',
    'sent' => 'Kami udah kirimin link untuk ganti password ke email kamu!',
    'token' => 'Kode untuk ganti passwordnya salah nih.',
    'user' => "Aneh, kok kami ga ketemu e-mail Kamu ya? Coba periksa lagi apakah emailnya udah bener atau belum",

];
