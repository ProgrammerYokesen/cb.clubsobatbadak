<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Email atau Password yang kamu masukkan salah. Silahkan pergunakan fitur lost password di bawah ini',
    'throttle' => 'Uppsss... kenapa mau login terus? Silahkan tunggu :seconds detik lagi.',

];
